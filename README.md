Personal Powerlevel 10k
===

## How to use
1. Clone repo under `$HOME`
```bash
git clone ssh://gitlab.com:nbugash/personal-powerlevel-10k ~/.personal-powerlevel-10k ;\
```
2. Add the `.p10k.zsh` file as part for the `.zshrc`
```bash
[[ -f $HOME/.personal-powerlevel-10k/.p10k.zsh ]] && echo "source $HOME/.personal-powerlevel-10k/.p10k.zsh" >> $HOME/.zshrc
```
